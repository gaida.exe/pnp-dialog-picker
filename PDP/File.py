# handles file loading

# package import
import os
from random import randrange
import subprocess


class File:
    rootDirectory = 'Files/'
    filePath = []
    lines = []

    # initial loading of the file
    def __init__(self, rootDirectory):
        self.rootDirectory = rootDirectory
        if not os.path.exists(rootDirectory):
            os.makedirs(rootDirectory)
            print(os.getcwd())
            os.startfile(os.getcwd() + '\\' + rootDirectory)
        self.updateFiles()

    # updates files if changes occurred
    def updateFiles(self):
        self.filePath.clear()
        self.filePath = os.listdir(self.rootDirectory)
        self.loadLines()

    # loads lines of the files in the directory
    def loadLines(self):
        self.lines.clear()
        for path in self.filePath:
            file = open(self.rootDirectory + path, 'r', encoding='utf-8')
            self.lines.append(file.readlines())

    # returns the names of the files for the buttons
    def getButtonNames(self):
        buttonNames = []
        for path in self.filePath:
            buttonNames.append(os.path.splitext(path)[0])

        return buttonNames

    # picks a random line and returns it
    def getRandomLineAtIndex(self, index):
        if len(self.lines[index]) > 0:
            return self.lines[index][randrange(len(self.lines[index]))]
        else:
            return ''

    # returns the length of filePath
    def getNumberOfFilePaths(self):
        return len(self.filePath)
