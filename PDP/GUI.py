# handles the GUI

# package import
import tkinter as tk


class GUI:
    root = []
    label = []
    canvas = []
    buttons = []
    width = 0
    height = 0
    wrapLength = 0
    buttonOffset = 0

    def __init__(self, title, numberOfButtons, buttonOffset, height):
        width = 100 + numberOfButtons * buttonOffset

        self.width = width if width > 400 else 400
        self.height = height if height > 400 else 400

        self.root = tk.Tk()
        self.root.title(string=title)
        self.label = tk.Label(self.root, text='', wraplength=400, fg='green', font=('helvetica', 12, 'bold'))
        self.canvas = tk.Canvas(self.root, width=self.width, height=self.height)
        self.canvas.pack()
        self.wrapLength = self.width - 100
        self.buttonOffset = buttonOffset

    def loop(self):
        self.root.mainloop()

    def createButtons(self, file):
        self.buttons.clear()
        for name in file.getButtonNames():
            i = len(self.buttons)
            button = tk.Button(text=name, command=(lambda index=i: self.print_text(index, file)), bg='brown',
                               fg='white', width=15, height=1)
            self.canvas.create_window(100 + 125 * i, self.height - 50, window=button)
            self.buttons.append(button)

    def print_text(self, index, file):
        self.label.destroy()
        self.label = tk.Label(self.root, text=file.getRandomLineAtIndex(index), wraplengt=400, fg='green',
                              font=('helvetica', 12, 'bold'))
        self.canvas.create_window(self.width / 2, self.height / 2, window=self.label)
