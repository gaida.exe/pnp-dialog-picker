#!/usr/bin/python
# -*- coding: utf-8 -*-

# own file import
import File
import GUI

if __name__ == '__main__':

    file = File.File('Files/')
    numberOfButtons = file.getNumberOfFilePaths()
    gui = GUI.GUI('PnP-Dialog-Picker', numberOfButtons, 125, 400)
    gui.createButtons(file)

    gui.loop()
