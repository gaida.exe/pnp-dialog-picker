# PnP-Dialog-Picker

### What is this program ?
This is a random dialog picker for any pen&paper to assist GMs and players.<br>

Just create a txt file in the `\Files`-directory and add every dialog as a single line to it.<br>
The program will read it out and create a button for every text file.<br>
The button then picks a random dialog to show.<br>

### How to install/compile?
You need to install python 3.6+ and pip, before you can just hit the `compile.bat`.<br>
Then you should run the `compile.bat` in something like git-bash, so you know what is going on.<br>
It then should create some files and you can just pick the `PDP.exe` from the `\dist`-directory.<br>
But don't forget to create a `\Files`-directory in the same level as the exe, where your text is.<br>

### Is there a compiled version?
I added a compiled version to the directory in the `PDP.zip`, but i would suggest you complie it yourself.<br>
**Why?** Because i can't guarantee that the code will work on your machine.<br>

### Any questions or feature requests?
Create an issue or contact me privatly, if you know how to reach me.
